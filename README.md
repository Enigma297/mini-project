# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* A Contact Book to store the data of the user using nodejs and express framework and storing the data using MongoDB's Mongoose interface.
* Version

### How do I get set up? ###

* Set up express, Mongoose, body-parser and nodemon using npm install MongoDB, install NodeJS
* Add a database named example 
* Run tests using basic mongoDb queries or use Postman
* N/A

### Contribution guidelines ###

* The code is a blend of css html5 nodeJS Express Mongoose and MongoDB running in the back.

### Who do I talk to? ###

* Repo owner or admin
